var requestSRC = './data/crm.json';

var request = new XMLHttpRequest();
request.open('GET', requestSRC, false);
request.send();
var billy = JSON.parse(request.responseText);

function populateDiv(jsonObj) {
    var template = `
    <div class="card"> 
        <div class="card-description"> 
            <div class="img">
                <img src="{{picture}}"> 
            </div>
            <div class="description">
                <h3>{{title}}</h3>
                <ul>
                    <li>{{first_name}}</li>
                    <li>{{last_name}}</li>
                    <li>{{birthday}}</li>
                    <li>{{email}}</li>
                    <li>{{phone}}</li>
                    <li>{{company}}</li>
                    <li>{{description}}</li>
                </ul>
            </div>
        </div>
        <div class="card-notes-tasks">
            <div class="notes">
                <h3>Notes</h3>
                <ul>
                    {{#notes}}
                    <li>{{subject}}</li>
                    <li>{{note}}</li>
                    <li>{{related_to}}</li>
                    {{/notes}}
                </ul>
            </div>
            <div class="tasks">
                <h3>Tasks</h3>
                <ul>
                    {{#tasks}}
                    <li>{{task}}</li>
                    <li>{{category}}</li>
                    <li>{{date}}</li>
                    <li>{{owner}}</li>
                    <li>{{priority}}</li>
                    <li>{{status}}</li>
                    <li>{{related_to}}</li>
                    <li>{{related_deal}}</li>
                    <p> </p>
                    {{/tasks}}
                </ul>
            </div>
        </div>
        <div class="card-tags">
            <h3>Tags</h3>
            <ul>
                {{#tags}}
                {{.}}
                {{/tags}}
            </ul>
        </div>
    </div>`

    /*console.log(jsonObj)*/

    jsonObj.customers.forEach(function (perso) {
        console.log(perso);
        Mustache.parse();
        var rendered = Mustache.render(template, perso);
        $("#app").append(rendered); 
    });
}

populateDiv(billy);

